import traceback
import multiprocessing
import re
import os
import zmq
import struct
import time

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib


def on_message(bus: Gst.Bus, message: Gst.Message, loop: GLib.MainLoop):
    msg_type = message.type

    # handle different types of messages
    if msg_type == Gst.MessageType.EOS:
        print("End of stream")
        loop.quit()
    elif msg_type == Gst.MessageType.ERROR:
        error, debug_info = message.parse_error()
        print(f"Error: {error.message}, Debug Info: {debug_info}")
        loop.quit()
    elif msg_type == Gst.MessageType.WARNING:
        warning, debug_info = message.parse_warning()
        print(f"Warning: {warning.message}, Debug Info: {debug_info}")

    return True

def on_handoff(element, buffer, data):
    # print(type(element), type(buffer))
    print("Handoff callback at id1!", "yellow")

    # map the buffer for reading
    result, mapinfo = buffer.map(Gst.MapFlags.READ)
    if not result:
        print("Mapping failed")
        return Gst.PadProbeReturn.OK

    # get the marker bit of the RTP header
    marker_bit = (mapinfo.data[1] & 0x80) >> 7
    if marker_bit: # the last fragment has the marker bit set
        print("This is the last fragment of the frame.", "red")

        ts_1_bytes = mapinfo.data[-8:]
        # unpack the id from bytes to a number
        ts_1 = struct.unpack('!Q', ts_1_bytes)[0]
        # print(colored(f"After reception: {frame_id}", "yellow"))
        
        id_bytes = mapinfo.data[-16:-8]
        # unpack the id from bytes to a number
        frame_id = struct.unpack('!Q', id_bytes)[0]

        # add a reference timestamp meta to the buffer
        reference = Gst.Caps.from_string("image/jpeg")
        meta = buffer.add_reference_timestamp_meta(reference, frame_id, Gst.CLOCK_TIME_NONE)

        # get current time, i.e., second timestamp
        ts_2 = time.time_ns()
        # record to dictionary
        data[frame_id] = [ts_1, ts_2]
        print(f"Frame ID: {frame_id}\nTimestamp 1: {ts_1}\nTimestamp 2: {ts_2}", "red")

        # remove last 16 bytes
        buffer.set_size(buffer.get_size() - 16)

    buffer.unmap(mapinfo)


def probe_callback(pad, info, data):
    print("Probe callback at id2!", "green")
    buffer = info.get_buffer()

    # map the buffer for reading
    result, mapinfo = buffer.map(Gst.MapFlags.READ)
    if not result:
        print("Mapping failed")
        return Gst.PadProbeReturn.OK

    # get the marker bit of the RTP header
    marker_bit = (mapinfo.data[1] & 0x80) >> 7

    buffer.unmap(mapinfo)

    if marker_bit: # the last fragment has the marker bit set
        print("This is the last fragment of the frame.", "green")
        # remove the probe to avoid recursion
        pad.remove_probe(info.id)
        
        reference = Gst.Caps.from_string("image/jpeg")
        # Retrieve the reference timestamp meta from the buffer
        meta = buffer.get_reference_timestamp_meta(reference)
        
        if meta:# Access the timestamp value
            frame_id = meta.timestamp
            ts_3 = time.time_ns()

            try:
                ts_1, ts_2 = data[frame_id]
                del data[frame_id]
            except KeyError:
                ts_1 = 0
                ts_2 = 0

            # '!Q' is the format for a big-endian unsigned long long
            id_bytes = struct.pack('!Q', frame_id)
            # create a new buffer with the id bytes as its data
            id_buffer = Gst.Buffer.new_wrapped(id_bytes)
            # append the new buffer to the original buffer
            tmp_buffer = buffer.append(id_buffer)

            # append ts_1
            ts_1_bytes = struct.pack('!Q', ts_1)
            ts_1_buffer = Gst.Buffer.new_wrapped(ts_1_bytes)
            tmp_1_buffer = tmp_buffer.append(ts_1_buffer)

            # append ts_2
            ts_2_bytes = struct.pack('!Q', ts_2)
            ts_2_buffer = Gst.Buffer.new_wrapped(ts_2_bytes)
            tmp_2_buffer = tmp_1_buffer.append(ts_2_buffer)

            # append ts_3
            ts_3_bytes = struct.pack('!Q', ts_3)
            ts_3_buffer = Gst.Buffer.new_wrapped(ts_3_bytes)
            new_buffer = tmp_2_buffer.append(ts_3_buffer)

            print(f"Frame ID: {frame_id}\nTimestamp 1: {ts_1}\nTimestamp 2: {ts_2}\nTimestamp 3: {ts_3}")

            # push the new buffer
            pad.push(new_buffer)

            # add the probe back
            pad.add_probe(Gst.PadProbeType.BUFFER, probe_callback, data)

            # tell GStreamer to drop the old buffer
            return Gst.PadProbeReturn.DROP

    return Gst.PadProbeReturn.OK

def sample(pipeline_desc: str, args:dict):
    print(f'Receiving MJPG video stream at UDP port {args["port"]}')
    print(f'Sampling rate: {args["framerate"]} frames/s')
    print(f'Sending samples at {args["dest"]}:{args["dport"]}')

    timestamps = dict() # {frame_id: [ts_1, ts_2, ...]}

    # create a new pipeline based on command line syntax
    pipeline = Gst.parse_launch(pipeline_desc)

    # retrieve the bus associated with the pipeline
    bus = pipeline.get_bus()
    # allow bus to emit signals for events
    bus.add_signal_watch()

    identity1 = pipeline.get_by_name("id1")
    # connect the callback to the "handoff" signal of identity1
    identity1.connect("handoff", on_handoff, timestamps)

    identity2 = pipeline.get_by_name("id2")
    identity2.get_static_pad('src').add_probe(Gst.PadProbeType.BUFFER, probe_callback, timestamps)


    # start pipeline
    pipeline.set_state(Gst.State.PLAYING)

    # create main event loop
    loop = GLib.MainLoop()
    # add callback to specific signal
    bus.connect("message", on_message, loop)

    try:
        loop.run()
    except KeyboardInterrupt:
        print('\nTerminating...')
    except Exception as e:
        print(f"Exception: {e}")
        # print exception information and stack trace entries
        traceback.print_exc()
    finally:
        # stop pipeline
        pipeline.set_state(Gst.State.NULL)
        loop.quit()


def update_rate(pipeline_desc:str, 
                    gst_process: multiprocessing.Process, 
                    args: dict):
    # control port
    c_port = args["control_port"]

    context = zmq.Context()
    # create REP socket
    rate_socket =  context.socket(zmq.REP)
    # bind socket to port
    rate_socket.bind(f'tcp://*:{c_port}')
    
    # create a poller and register the socket for polling
    poller = zmq.Poller()
    poller.register(rate_socket, zmq.POLLIN)
   
    print(f"Listening for interval update requests on port {c_port}")
   
    while True:
        try:
            # poll for events
            events = dict(poller.poll())
            # check for events on rate_socket
            if rate_socket in events and events[rate_socket] == zmq.POLLIN:
                new_rate = rate_socket.recv_string()
                print(f"Received new sampling rate value: {new_rate} frames/s")

                # kill the child process
                gst_process.terminate()
                # wait for child process to finish
                gst_process.join()
                # use regular expression to replace value for frame-rate
                pipeline_desc = re.sub(
                    r'framerate=\d+/\d+',
                    f'framerate={new_rate}', 
                    pipeline_desc
                )
                # print(pipeline_desc)
                # update args
                args["framerate"] = new_rate
                # start a separate process running the gstreamer pipeline
                gst_process = multiprocessing.Process(
                    target=sample, args=(pipeline_desc, args))
                gst_process.start()

                # send a response back to the client if needed
                rate_socket.send_string(f"Changed sampling rate to {new_rate} frames/s")
        except KeyboardInterrupt:
            # kill the child process
            gst_process.terminate()
            # wait for child process to finish
            gst_process.join()
            break


if __name__ == '__main__':
    args = {'port': os.getenv('port'), 
        'framerate': os.getenv('framerate'),
        'dest': os.getenv('destination_ip'),
        'dport': os.getenv('destination_port'),
        'control_port': os.getenv('framerate_port')
    }

    # initialize the gstreamer library
    Gst.init(None) 

    pipeline_desc = (
        f'udpsrc port={args["port"]} ! '
        'application/x-rtp, encoding-name=JPEG, payload=26 ! '
        'identity name=id1 ! '
        'queue ! '
        'rtpjpegdepay ! '
        'queue ! '
        'jpegparse ! '
        'videorate ! '
        f'image/jpeg, framerate={args["framerate"]} ! '
        'queue ! '
        'rtpjpegpay ! '
        'queue ! '
        'identity name=id2 ! '
        f'udpsink host={args["dest"]} port={args["dport"]} sync=False'
    )

    gst_process = multiprocessing.Process(
        target=sample, args=(pipeline_desc, args))
    gst_process.start()

    update_rate(pipeline_desc, gst_process, args)